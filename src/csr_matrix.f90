      module csr_matrix

      use omp_lib

      type CSR_SYM_Matrix
            integer :: n                              ! dim
            integer :: len                            ! none zero elements count
            integer , pointer :: ig(:)                ! rows
            integer , pointer :: jg(:)                ! cols
            double precision , pointer :: di(:)       ! diag
            double precision , pointer :: ggu(:)      ! upper triangle
            double precision , pointer :: ggl(:)      ! lower triangle
      end type CSR_SYM_Matrix

      private :: DotProdCSR, ProdVSCSR
      public  :: Allocate_CSR_SYM_Matrix, Deallocate_CSR_SYM_Matrix
      public  :: MatMulCSR, MatMul2CSR, MatMul3CSR
      public  :: LinearCombination

      contains

      subroutine Allocate_CSR_SYM_Matrix(A, n, len)
            integer :: n, len
            type(CSR_SYM_Matrix) A

            integer :: ierr

            A%n = n
            A%len = len

            allocate(A%ig(A%n+1), STAT=ierr)
            if (ierr/=0) stop "Can't allocate memory for rows index."

            allocate(A%jg(A%len), STAT=ierr)
            if (ierr/=0) stop "Can't allocate memory for cols index."

            allocate(A%di(A%n), STAT=ierr)
            if (ierr/=0) stop "Can't allocate memory for diag."

            allocate(A%ggl(A%len), STAT=ierr)
            if (ierr/=0) stop "Can't allocate memory for lower triangle."
            A%ggu=>A%ggl

      end subroutine Allocate_CSR_SYM_Matrix

      subroutine Deallocate_CSR_SYM_Matrix(A)
            type(CSR_SYM_Matrix) A

            deallocate(A%ig, A%jg)
            nullify(A%ggu)
            deallocate(A%di, A%ggl)

      end subroutine Deallocate_CSR_SYM_Matrix

      ! y = beta * y + alpha * (CSR A) * x
      ! Nflop = 3 * A%n + 2 * A%len
      subroutine LinearCombination(beta, y, alpha, A, x)
            type(CSR_SYM_Matrix), intent(in) :: A
            double precision, intent(inout) :: beta, alpha, x(:)
            double precision, intent(inout) :: y(:)
            integer :: i, k, j
            double precision :: sum

            !$omp parallel private(i, j, k, sum) shared(beta, y, alpha, A, x)

            !$omp do
            do i = 1, A%n
                  y(i) = beta * y(i)
                  x(i) = alpha * x(i)
            end do
            !$omp end do

            !$omp do
            do i = 1, A%n
                  sum = A%di(i) * x(i)
                  do k = A%ig(i), A%ig(i+1) - 1
                        j = A%jg(k)
                        sum = sum + A%ggl(k) * x(j)
                        !$omp atomic
                        y(j) = y(j) + A%ggu(k) * x(i)
                  end do

                  !$omp atomic
                  y(i) = y(i) + sum
            end do
            !$omp end do
            !$omp end parallel

      end subroutine LinearCombination

      ! CSR A * x = f
      ! Nflop = A%n + 2 * A%len
      subroutine MatMul3CSR(A, x, f)
            type(CSR_SYM_Matrix), intent(in) :: A
            double precision, intent(in)  :: x(:)
            double precision, intent(out) :: f(:)
            integer :: i, k, j
            double precision :: sum

            !$omp parallel do private(i, k, j, sum) shared(A, x, f)
            do i = 1, A%n
                  sum = A%di(i) * x(i)
                  do k = A%ig(i), A%ig(i+1) - 1
                        j = A%jg(k)
                        sum = sum + A%ggl(k) * x(j)
                        !$omp atomic
                        f(j) = f(j) + A%ggu(k) * x(i)
                  end do

                  !$omp atomic
                  f(i) = f(i) + sum
            end do
            !$omp end parallel do

      end subroutine MatMul3CSR

      ! CSR A * x = f
      ! Nflop = A%n + 2 * A%len
      subroutine MatMul2CSR(A, x, f)
            type(CSR_SYM_Matrix), intent(in) :: A
            double precision, intent(in)  :: x(:)
            double precision, intent(out) :: f(:)
            integer :: i, k, j

            !$omp parallel do private(i, k, j) shared(A, x, f)
            do i = 1, A%n
                  !$omp atomic
                  f(i) = f(i) + A%di(i) * x(i)
                  do k = A%ig(i), A%ig(i+1) - 1
                        j = A%jg(k)
                        !$omp atomic
                        f(i) = f(i) + A%ggl(k) * x(j)
                        !$omp atomic
                        f(j) = f(j) + A%ggu(k) * x(i)
                  end do
            end do
            !$omp end parallel do

      end subroutine MatMul2CSR

      ! CSR A * x = f
      ! Nflop = A%n + 2 * A%len
      subroutine MatMulCSR(A, x, f)
            type(CSR_SYM_Matrix), intent(in) :: A
            double precision, intent(in)  :: x(:)
            double precision, intent(out) :: f(:)
            integer :: i, k

            !$omp parallel do private(i, k) shared(A, x, f)
            do i = 1, A%n
                  !$omp atomic
                  f(i) = f(i) + A%di(i) * x(i)
                  k = A%ig(i + 1) - A%ig(i)
                  !$omp atomic
                  f(i) = f(i) + DotProdCSR(A%ggl(A%ig(i):), x, A%jg(A%ig(i):), k)
                  call ProdVSCSR(A%ggu(A%ig(i):), x(i), f, A%jg(A%ig(i):), k)
            end do
            !$omp end parallel do

      end subroutine MatMulCSR

      double precision function DotProdCSR(a, x, jg, k)
            double precision, intent(in) :: a(:),x(:)
            integer, intent(in) :: jg(:), k
            double precision :: s
            integer :: i

            s = 0d0
            do i = 1, k
                  s = s + a(i) * x(jg(i))
            end do

            DotProdCSR = s
            return
      end function DotProdCSR

      !     f = f + a * x
      subroutine ProdVSCSR(a, x, f, jg, k)
            double precision, intent(in)  :: a(:), x
            double precision, intent(out) :: f(:)
            integer, intent(in) :: jg(:), k
            integer :: i

            do i = 1, k
                  !$omp atomic
                  f(jg(i)) = f(jg(i)) + a(i) * x
            end do

            return
      end subroutine ProdVSCSR

      end module csr_matrix
