module generate_matrix
    use csr_matrix
    use omp_lib
    implicit none

contains

    ! Test 1
    ! y = beta * y + alpha * A * x
    ! y = (/ (1, i = 1, n) /)
    ! x = (/ (1, i = 1, n) /)
    ! alpha = beta = 0.5
    ! A = I (n x n matrix)
    ! result y = (/ (1, i = 1, n) /)
    subroutine test1_LinearCombination(n)
        implicit none
        integer, intent(in) :: n
        type(CSR_SYM_Matrix) :: A
        double precision, allocatable :: x(:), y(:)
        double precision :: alpha, beta
        double precision :: time, relativeError
        integer :: i

        allocate(x(n), y(n))
        call generate_In(A, n)
        x = 1d0
        y = 1d0
        alpha = 0.5d0
        beta = 0.5d0

        print *, "Start y = beta * y + alpha * A * x, A = I(identity matrix)"
        time = -omp_get_wtime()
        call LinearCombination(beta, y, alpha, A, x)
        time = time + omp_get_wtime()

        x = (/ (1, i = 1, n) /)
        x = x - y
        relativeError = dsqrt(dot_product(x, x))/dsqrt(1d0 * 1d0 * n)
        print *, "Relative error = ", relativeError
        print *, "Gflops = ", (3 * A%n + 2 * A%len) / time / 1d9
        print *, "Threads = ", omp_get_max_threads(), " time = ", time

        call Deallocate_CSR_SYM_Matrix(A)
        deallocate(x, y)

    end subroutine test1_LinearCombination

    ! Test 2
    ! y = beta * y + alpha * A * x
    ! y = (/ (1, i = 1, n) /)
    ! x = (/ (1, i = 1, n) /)
    ! alpha = beta = 0.5
    ! A (n x n tridiagonal matrix)
    ! result y = (/ 2, (2.5, i = 1, n - 2), 2 /)
    subroutine test2_LinearCombination(n)
        implicit none
        integer, intent(in) :: n
        type(CSR_SYM_Matrix) :: A
        double precision, allocatable :: x(:), y(:)
        double precision :: alpha, beta
        double precision :: time, relativeError

        allocate(x(n), y(n))
        call generate_Bn(A, n)
        x = 1d0
        y = 1d0
        alpha = 0.5d0
        beta = 0.5d0

        print *, "Start y = beta * y + alpha * A * x, A (tridiagonal matrix)"
        time = -omp_get_wtime()
        call LinearCombination(beta, y, alpha, A, x)
        time = time + omp_get_wtime()

        x = (/ 2d0, spread(2.5d0, 1, n - 2), 2d0 /)
        x = x - y
        relativeError = dsqrt(dot_product(x, x))/dsqrt(2d0 * 2d0 * 2 + 2.5d0 * 2.5d0 * (n - 2))
        print *, "Relative error = ", relativeError
        print *, "Gflops = ", (3 * A%n + 2 * A%len) / time / 1d9
        print *, "Threads = ", omp_get_max_threads(), " time = ", time

        call Deallocate_CSR_SYM_Matrix(A)
        deallocate(x, y)

    end subroutine test2_LinearCombination

    ! Test 3
    ! f = A * x
    ! x = (/ (1, i = 1, n) /)
    ! A (n x n tridiagonal matrix)
    ! result f = (/ 3, (4, i = 1, n), 3 /)
    subroutine test3_MatMul2CSR(n)
        implicit none
        integer, intent(in) :: n
        type(CSR_SYM_Matrix) :: A
        double precision, allocatable :: x(:), f(:)
        double precision :: time, relativeError
        integer :: i

        allocate(x(n), f(n))
        call generate_Bn(A, n)
        x = 1d0
        f = 0d0

        print *, "Start f = A * x (MatMul2CSR)"
        time = -omp_get_wtime()
        call MatMul2CSR(A, x, f)
        time = time + omp_get_wtime()

        x = (/ 3, (4, i = 1, n - 2), 3 /)
        x = x - f
        relativeError = dsqrt(dot_product(x, x))/dsqrt(3d0 * 3d0 * 2 + 2d0 * 2d0 * (n - 2))

        print *, "Relative error = ", relativeError
        print *, "Gflops = ", (A%n + 2 * A%len) / time / 1d9
        print *, "Threads = ", omp_get_max_threads(), " time = ", time

        call Deallocate_CSR_SYM_Matrix(A)
        deallocate(x, f)

    end subroutine test3_MatMul2CSR

    ! Test 4
    ! f = A * x
    ! x = (/ (1, i = 1, n) /)
    ! A (n x n tridiagonal matrix)
    ! result f = (/ 3, (4, i = 1, n), 3 /)
    subroutine test4_MatMul3CSR(n)
        implicit none
        integer, intent(in) :: n
        type(CSR_SYM_Matrix) :: A
        double precision, allocatable :: x(:), f(:)
        double precision :: time, relativeError
        integer :: i

        allocate(x(n), f(n))
        call generate_Bn(A, n)
        x = 1d0
        f = 0d0

        print *, "Start f = A * x (MatMul3CSR)"
        time = -omp_get_wtime()
        call MatMul3CSR(A, x, f)
        time = time + omp_get_wtime()

        x = (/ 3, (4, i = 1, n - 2), 3 /)
        x = x - f
        relativeError = dsqrt(dot_product(x, x))/dsqrt(3d0 * 3d0 * 2 + 2d0 * 2d0 * (n - 2))

        print *, "Relative error = ", relativeError
        print *, "Gflops = ", (A%n + 2 * A%len) / time / 1d9
        print *, "Threads = ", omp_get_max_threads(), " time = ", time

        call Deallocate_CSR_SYM_Matrix(A)
        deallocate(x, f)

    end subroutine test4_MatMul3CSR

    ! Test 5
    ! f = A * x
    ! x = (/ (1, i = 1, n) /)
    ! A (n x n tridiagonal matrix)
    ! result f = (/ 3, (4, i = 1, n), 3 /)
    subroutine test5_MatMulCSR(n)
        implicit none
        integer, intent(in) :: n
        type(CSR_SYM_Matrix) :: A
        double precision, allocatable :: x(:), f(:)
        double precision :: time, relativeError
        integer :: i

        allocate(x(n), f(n))
        call generate_Bn(A, n)
        x = 1d0
        f = 0d0

        print *, "Start f = A * x (MatMulCSR)"
        time = -omp_get_wtime()
        call MatMulCSR(A, x, f)
        time = time + omp_get_wtime()

        x = (/ 3, (4, i = 1, n - 2), 3 /)
        x = x - f
        relativeError = dsqrt(dot_product(x, x))/dsqrt(3d0 * 3d0 * 2 + 2d0 * 2d0 * (n - 2))

        print *, "Relative error = ", relativeError
        print *, "Gflops = ", (A%n + 2 * A%len) / time / 1d9
        print *, "Threads = ", omp_get_max_threads(), " time = ", time

        call Deallocate_CSR_SYM_Matrix(A)
        deallocate(x, f)

    end subroutine test5_MatMulCSR

    ! Identity matrix
    subroutine generate_In(I, n)
        implicit none
        type(CSR_SYM_Matrix), intent(out) :: I
        integer, intent(in) :: n

        call Allocate_CSR_SYM_Matrix(I, n, 0)

        I%di = 1d0
        I%ggl = 0d0
        I%ig = 1
        I%jg = 0

    end subroutine generate_In

    ! Band matrix(Tridiagonal matrix)
    subroutine generate_Bn(B, n)
        implicit none
        type(CSR_SYM_Matrix), intent(out) :: B
        integer, intent(in) :: n
        integer :: k

        call Allocate_CSR_SYM_Matrix(B, n, n-1)

        B%di = 2d0
        B%ggl = 1d0
        B%ig(1) = 1
        B%ig(2:) = (/ (k, k = 1, n) /)
        B%jg = (/ (k, k = 1, n-1) /)

    end subroutine generate_Bn

end module generate_matrix