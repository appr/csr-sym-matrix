
program CSR_test_proj

      use csr_matrix
      use generate_matrix
      use omp_lib

      implicit none
      integer :: n

      n = 100d6

      call test1_LinearCombination(n)

      call test2_LinearCombination(n)

      call test5_MatMulCSR(n)

      call test3_MatMul2CSR(n)

      call test4_MatMul3CSR(n)

end program CSR_test_proj
