cmake_minimum_required(VERSION 3.0)

project(CSR_test)
enable_language(Fortran)

if(CMAKE_Fortran_COMPILER_ID MATCHES "GNU")
    set(dialect "-ffree-form -std=f2008 -fimplicit-none -fopenmp")
endif()
if(CMAKE_Fortran_COMPILER_ID MATCHES "Intel")
    set(dialect "-stand f08 -free -implicitnone")
endif()
if(CMAKE_Fortran_COMPILER_ID MATCHES "PGI")
    set(dialect "-Mfreeform -Mdclchk -Mstandard -Mallocatable=03")
endif()

list(APPEND CMAKE_Fortran_FLAGS ${dialect})

file(GLOB_RECURSE sources  src/*.f90 src/*.h)
add_executable(csr_test ${sources})